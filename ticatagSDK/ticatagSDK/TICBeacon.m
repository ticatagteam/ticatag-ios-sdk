//
//  TICBeacon.m
//  tibeacon
//
//  Created by Fred Visticot on 16/02/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import "TICBeacon.h"
#import <CoreLocation/CoreLocation.h>

@implementation TICBeacon
{
}

-(id)initWithBeacon:(CLBeacon *)beacon
{
    if ( ( self = [super init] ) )
	{
        _major=beacon.major;
        _minor=beacon.minor;
        _proximity=beacon.proximity;
        _accuracy=beacon.accuracy;
        _rssi=beacon.rssi;
        _proximityUUID=beacon.proximityUUID;
	}
	return self;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"ProximityUUID: %@ Major: %@ Minor: %@ Measured power: %@", [_proximityUUID UUIDString],  _major, _minor, _measuredPower];
}

@end
