//
//  ticatagSDK.h
//  ticatagSDK
//
//  Created by Fred Visticot on 01/03/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ticatag/TICBeacon.h>
#import <ticatag/TICBeaconRegion.h>
#import <ticatag/TICBeaconManager.h>


@interface ticatagSDK : NSObject

@end
