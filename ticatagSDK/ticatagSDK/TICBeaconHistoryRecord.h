#import <Foundation/Foundation.h>


@interface TICBeaconHistoryRecord : NSObject 

@property (nonatomic, retain) NSString* proximityUUID;
@property (nonatomic) double longitude;
@property (nonatomic) double latitude;
@property (nonatomic) int batteryLevel;
@property (nonatomic) int major;
@property (nonatomic) int minor;
@property (nonatomic) NSString *deviceOs;
@property (nonatomic) NSString *deviceName;
@property (nonatomic) NSString *networkAccess;
@property (nonatomic) NSString *appId;
@property (nonatomic) NSString *apiVersion;
@property (nonatomic) NSString *devicePlatform;

@end
