//
//  TTStatisticService.m
//  ticatagapi
//
//  Created by Fred Visticot on 7/31/13.
//  Copyright (c) 2013 ticatag. All rights reserved.
//

#import "TICStatisticService.h"
#import "TICBeaconHistoryRecord.h"
#import "Reachability.h"
#import "UIDevice-Hardware.h"
#import <AdSupport/ASIdentifierManager.h>
#import "TICBeaconHistoryRecord.h"
#import "TICBeacon.h"
#import "TICBeaconRegion.h"
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>

#define FLUSH_MODULO    5

@interface TICStatisticService()
@property(nonatomic, strong) NSString *deviceName;
@property(nonatomic, strong) NSString *OSName;
@property(nonatomic, strong) NSString *OSVersion;
@property(nonatomic, strong) NSString *platform;
@property(nonatomic, strong) NSString *deviceId;
@property(nonatomic, strong) NSString *locale;
@property(nonatomic, strong) NSString *apiVersion;
@property(nonatomic, strong) NSString *appId;
@property(nonatomic) double longitude;
@property(nonatomic) double latitude;
@property(nonatomic, strong) NSMutableArray *records;
@property(nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation TICStatisticService


static TICStatisticService * _singleton;


+(TICStatisticService*)shared
{
    @synchronized(self) {
        if (_singleton == nil) {
        _singleton = [[self alloc] init];
        [_singleton start];
        _singleton.flushModulo=FLUSH_MODULO;
        }
    }
    return _singleton;
}


-(TICBeaconHistoryRecord*)createHistoricalRecord: (TICBeaconRegion*)region
{
    TICBeaconHistoryRecord *record = [[TICBeaconHistoryRecord alloc] init];
    record.proximityUUID=[region.proximityUUID UUIDString];
    record.major=[region.major integerValue];
    record.major=[region.minor integerValue];
    record.devicePlatform=@"IOS";
    record.deviceName=_deviceName;
    record.longitude=_longitude;
    record.latitude=_latitude;
    record.deviceOs=[NSString stringWithFormat:@"%@%@", _OSName, _OSVersion];
    //record.apiVersion= API_VERSION;
    [_records addObject: [self recordToDictionary: record]];
    return record;
}


-(void)start
{
    NSLog(@"Starting statistics service");
    _records = [NSMutableArray array];
    _locale = [[NSLocale currentLocale] localeIdentifier];
	UIDevice *device = [UIDevice currentDevice];
	_deviceName = [device model];
	_OSName = [device systemName];
	_OSVersion = [device systemVersion];
    //_platform = [device platformString];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self flush];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSLog(@"Entering background, flushing stats");
        [self flush];
    }];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    _latitude=location.coordinate.latitude;
    _longitude=location.coordinate.longitude;
    [manager stopUpdatingLocation];
}

-(void)checkFlush
{
    if (([_records count] > 0) && ([_records count] % _flushModulo == 0))
    {
        [self flush];
    }
}

-(NSString *)networkAccess {
	NSString *networkAccess;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == ReachableViaWiFi)
    {
        networkAccess = @"Wifi";
    }
    else if(internetStatus == ReachableViaWWAN)
    {
        networkAccess = @"WWAN";
    }
    else if (internetStatus == NotReachable) 
    {
        networkAccess = @"notreachable";
    }
    return networkAccess;
}

-(NSString*)userAgent {
    NSString *networkAccess = [self networkAccess];
    
    NSString *userAgent =  [NSString stringWithFormat:@"api-version=%@;app-id=%@;device=%@;os=%@ %@;locale=%@;network=%@", _apiVersion, _appId, _platform, _OSName, _OSVersion, _locale, networkAccess];
    return userAgent;
}

-(void)stop
{
    
}

-(void)flush
{
    NSLog(@"Flushing statistics to server");
    [self uploadToServer];
}


-(NSDictionary*)recordToDictionary: (TICBeaconHistoryRecord*)record
{
    NSDictionary *dico = [NSDictionary dictionaryWithObjectsAndKeys:
                          record.proximityUUID, @"proximity_uuid",
                          [NSNumber numberWithInt:record.major], @"major",
                          [NSNumber numberWithInt:record.minor], @"minor",
                          [NSNumber numberWithDouble: record.latitude], @"latitude",
                          [NSNumber numberWithDouble: record.longitude], @"longitude",
                          [NSNumber numberWithInt:record.batteryLevel], @"battery_level",
                          record.deviceOs, @"device_os",
                          record.devicePlatform, @"device_platform",
                          record.deviceName, @"device_name",
                          record.appId, @"app_id",
                          record.apiVersion, @"api_version"
                          , nil];
    return dico;
}

-(void)uploadToServer
{
    
    if ([[self networkAccess] isEqualToString: @"notreachable"])
    {
        NSLog(@"Network not available, can not send statistics");
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"fred" password:@"fred"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:@"http://ticatag.com:8280/api/stats" parameters:_records success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        [_records removeAllObjects];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}




@end
