//
//  TICBeaconManager.m
//  tibeacon
//
//  Created by Fred Visticot on 16/02/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import "TICBeaconManager.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "TICBeaconRegion.h"
#import "TICBeacon.h"
#import "TICStatisticService.h"
#import "BLEDevice.h"
#import "BLETIOADProfile.h"
#import <AFNetworking/AFNetworking.h>


#define kBeaconMinorValueCharacteristicUUID         @"ff11"
#define kBeaconMajorValueCharacteristicUUID         @"ff22"
#define kBeaconProximityUUIDCharacteristicUUID      @"ff33"
#define kBeaconAdvTxPowerCharacteristicUUID         @"ff52"
#define kBeaconTxPowerCharacteristicUUID            @"ff54"
#define kBeaconAdvOnIntervalCharacteristicUUID      @"ff65"
#define kBeaconAdvOffIntervalCharacteristicUUID     @"ff76"
#define kBeaconAdvIntervalCharacteristicUUID        @"ff87"
#define kBeaconAdvModeCharacteristicUUID            @"ff97"

static const NSTimeInterval CALIBRATION_DURATION = 20.0f;
static const NSTimeInterval CALIBRATION_PERIOD = 1.0f;


@interface DeviceInformation: NSObject
@property (nonatomic) NSNumber *major;
@property (nonatomic) NSNumber *minor;
@property (nonatomic, strong) NSString *macAddress;
@property (nonatomic) int power;
@end

@implementation DeviceInformation

-(NSString*)description {
    return [NSString stringWithFormat:@"MacAddress: %@ Major: %@ Minor: %@ Power: %d", _macAddress, _major, _minor, _power];
}

@end

@interface TICBeaconManager() <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (copy, nonatomic) TICBeaconManagerCompletionBlock initBlock;
@property (copy, nonatomic) TICBeaconDisconnectCompletionBlock beaconDisconnectCompletionBlock;
@property (copy, nonatomic) TICBeaconConnectCompletionBlock beaconConnectCompletionBlock;
@property (copy, nonatomic) TICBeaconManagerCompletionBlock beaconUpdateCompletionBlock;
@property (copy, nonatomic) TICUpdateFirmwareProgressHandler beaconUpdateFirmwareProgressBlock;
@property (copy, nonatomic) TICUpdateFirmwareCompletionBlock beaconUpdateFirmwareCompletionBlock;
@property (copy, nonatomic) TICCalibrationProgressHandler beaconCalibrationProgressBlock;
@property (copy, nonatomic) TICCalibrationCompletionBlock beaconCalibrationCompletionBlock;

@property(copy, nonatomic) CBCharacteristic *minorValueChar;
@property(copy, nonatomic) CBCharacteristic *majorValueChar;
@property(copy, nonatomic) CBCharacteristic *proximityUUIDChar;
@property(copy, nonatomic) CBCharacteristic *measuredPowerChar;
@property(copy, nonatomic) CBCharacteristic *txPowerChar;
@property(copy, nonatomic) CBCharacteristic *advIntervalChar;
@property(copy, nonatomic) CBCharacteristic *advOnIntervalChar;
@property(copy, nonatomic) CBCharacteristic *advOffIntervalChar;
@property(copy, nonatomic) CBCharacteristic *advModeChar;
@property(copy, nonatomic) CBCharacteristic *battLevelChar;

@property(copy, nonatomic) CBCharacteristic *firmwareRevisionChar;
@property(copy, nonatomic) CBCharacteristic *hardwareRevisionChar;
@property(copy, nonatomic) CBCharacteristic *manufacturerChar;

@property (nonatomic) int nbProfileInfoRead;
@property (nonatomic) NSTimer *connectionTimer;
@property (nonatomic) NSTimer *scanTimer;
@property(nonatomic) BOOL connecting;
@property(nonatomic, strong) BLETIOADProfile *oadProfile;
@property(nonatomic, strong) TICBeacon *currentBeacon;

@property (nonatomic) BOOL isCalibrating;
@property (nonatomic, strong) NSMutableArray *calibrationRSSIs;
@property (nonatomic, strong) NSTimer *calibrationTimer;
@property (nonatomic, strong) NSDate *startCalibrationDate;

@end

@implementation TICBeaconManager
{
    
}

+ (TICBeaconManager *)shared
{
    static TICBeaconManager *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[self alloc]init];
    });
    
    return sharedController;
}


- (id) initWithCompletion: (TICBeaconManagerCompletionBlock)completion andDelegate:(id<TICBeaconManagerDelegate>)delegate
{
    if ( ( self = [super init] ) )
	{
        _initBlock=completion;
        [self initialize];
        self.delegate=delegate;
	}
	return self;
}


-(void)initialize
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    dispatch_queue_t centralQueue = nil;
    //centralQueue = dispatch_queue_create("com.ticatag.mycentral", DISPATCH_QUEUE_SERIAL);
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue: centralQueue];
}


-(void)startMonitoring
{
    if (_centralManager.state != CBCentralManagerStatePoweredOn) {
        return;
    }
    
    BOOL allowDuplicate = NO;
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool: allowDuplicate],
                             CBCentralManagerScanOptionAllowDuplicatesKey, nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_centralManager scanForPeripheralsWithServices: nil options:options];
    });
    _scanTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scanTimeout:) userInfo:nil repeats:NO];
}

-(void)scanTimeout: (NSTimer*)timer
{
    NSLog(@"ScanTimeout");
    [_centralManager stopScan];
}


- (void)requestStateForRegion:(TICBeaconRegion *)region
{
    [_locationManager requestStateForRegion: region];
}

- (void)startMonitoringForRegion:(TICBeaconRegion *)region
{
    [_locationManager startMonitoringForRegion: region];
    
}

- (void)stopMonitoringForRegion:(TICBeaconRegion *)region
{
    [_locationManager stopMonitoringForRegion: region];
}

- (void)startRangingBeaconsInRegion:(TICBeaconRegion *)region
{
    [_locationManager startRangingBeaconsInRegion: region];
}

- (void)stopRangingBeaconsInRegion:(TICBeaconRegion *)region
{
    [_locationManager stopRangingBeaconsInRegion: region];
}


#pragma mark -
#pragma mark CLLocationManagerDelegates

- (void)beaconManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(TICBeaconRegion *)region
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:didDetermineState:forRegion:)])
    {
        [_delegate beaconManager:self didDetermineState:state forRegion: region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(TICBeaconRegion *)region
{
    [[TICStatisticService shared] createHistoricalRecord: region];
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:didEnterRegion:)])
    {
        [_delegate beaconManager:self didEnterRegion: region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(TICBeaconRegion *)region
{
    [[TICStatisticService shared] createHistoricalRecord: region];
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:didExitRegion:)])
    {
        [_delegate beaconManager:self didExitRegion: region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(TICBeaconRegion *)region
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:didRangeBeacons:inRegion:)])
    {
        NSMutableArray *ttBeacons = [NSMutableArray arrayWithCapacity: beacons.count];
        for (CLBeacon *beacon in beacons) {
            [ttBeacons addObject: [[TICBeacon alloc] initWithBeacon: beacon]];
        }
        [_delegate beaconManager:self didRangeBeacons:ttBeacons inRegion:region];
    }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(TICBeaconRegion *)region withError:(NSError *)error
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:monitoringDidFailForRegion:withError:)])
    {
        [_delegate beaconManager:self monitoringDidFailForRegion:region withError:error];
    }
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(TICBeaconRegion *)region withError:(NSError *)error
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(beaconManager:rangingBeaconsDidFailForRegion:withError:)])
    {
        [_delegate beaconManager:self rangingBeaconsDidFailForRegion:region withError:error];
    }
}



#pragma mark -
#pragma mark CBCentralManagerDelegates
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *stateDesc=nil;
    
    switch(central.state) {
        case CBCentralManagerStateUnknown:
            stateDesc=@"State unknown (CBCentralManagerStateUnknown)";
            break;
        case CBCentralManagerStateResetting:
            stateDesc=@"State resetting (CBCentralManagerStateUnknown)";
            break;
        case CBCentralManagerStateUnsupported:
            stateDesc=@"State BLE unsupported (CBCentralManagerStateResetting)";
            break;
        case CBCentralManagerStateUnauthorized:
            stateDesc=@"State unauthorized (CBCentralManagerStateUnauthorized)";
            break;
        case CBCentralManagerStatePoweredOff:
            stateDesc=@"State BLE powered off (CBCentralManagerStatePoweredOff)";
            break;
        case CBCentralManagerStatePoweredOn:
            stateDesc=@"State powered up and ready (CBCentralManagerStatePoweredOn)";
            break;
        default:
            stateDesc=@"State unknown";
    }
    
    NSLog(@"Current state: %@", stateDesc);
    
    
    if (_initBlock != nil)
    {
        if (central.state == CBCentralManagerStatePoweredOn) {
            _initBlock(self, YES, nil);
        }
        else {
            _initBlock(self, NO, [NSError errorWithDomain: stateDesc code:0 userInfo:nil]);
        }
    }
}

#pragma mark -
#pragma mark BeaconProfileManagement



- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral
{
    NSLog(@"New name: %@", peripheral.name);
}


-(NSData*)reverseBytes: (NSData*)data
{
    const char *bytes = [data bytes];
    char *reverseBytes = malloc(sizeof(char) * [data length]);
    int index = [data length] - 1;
    for (int i = 0; i < [data length]; i++)
        reverseBytes[index--] = bytes[i];
    NSData *reversedData = [NSData dataWithBytes:reverseBytes length:[data length]];
    free(reverseBytes);
    return  reversedData;
}

-(DeviceInformation*)decodeDeviceInformation:(NSData*)data
{
    DeviceInformation *deviceInformation = [[DeviceInformation alloc] init];
    
    NSRange range = {0, 6};
    NSData *subData = [self reverseBytes: [data subdataWithRange: range]];
    
    NSUInteger capacity = [subData length] * 2;
    NSMutableString *macAddress = [NSMutableString stringWithCapacity:capacity];
    const unsigned char *dataBuffer = [subData bytes];
    NSInteger i;
    for (i=0; i<[subData length]; ++i) {
        [macAddress appendFormat:@"%02lX", (unsigned long)dataBuffer[i]];
    }
    deviceInformation.macAddress=macAddress;
    
    NSRange rangePower = {6, 1};
    int power;
    subData = [data subdataWithRange: rangePower];
    [subData getBytes:&power length:sizeof(power)];
    deviceInformation.power=power;
    
    NSRange rangeMajor = {7, 2};
    TICBeaconMajorValue major;
    subData = [self reverseBytes: [data subdataWithRange: rangeMajor]];
    [subData getBytes:&major length:sizeof(major)];
    deviceInformation.major=[NSNumber numberWithInt: major];
    
    NSRange rangeMinor = {9, 2};
    TICBeaconMinorValue minor;
    subData = [self reverseBytes: [data subdataWithRange: rangeMinor]];
    [subData getBytes:&minor length:sizeof(minor)];
    deviceInformation.minor= [NSNumber numberWithInt: minor];
    
    NSLog(@"DeviceInformation: %@", deviceInformation);
    
    return  deviceInformation;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    
    NSLog(@"Peripheral name: %@", peripheral.name);
    NSString *localName = [advertisementData objectForKey:@"kCBAdvDataLocalName"];
    
    if (_isCalibrating)
    {
        [_calibrationRSSIs addObject: RSSI];
        NSDate *now = [NSDate date];
        NSTimeInterval interval = [now timeIntervalSinceDate: _startCalibrationDate];
        double progress= interval / (NSTimeInterval)CALIBRATION_DURATION;
        _beaconCalibrationProgressBlock(progress);
        return;
    }
    
    NSDictionary *dicoService = [advertisementData objectForKey: @"kCBAdvDataServiceData"];
    for (CBUUID *key in [dicoService allKeys]) {
        if ([[key description] isEqualToString:@"Device Information"])
        {
            NSData *data =[dicoService objectForKey: key];
            NSLog(@"data: %@", data);
            if ([localName isEqualToString:@"ticatag"])
            {
                DeviceInformation *deviceInformation = [self decodeDeviceInformation: data];
                if ([_currentBeacon.major intValue]  == [deviceInformation.major intValue] && [_currentBeacon.minor intValue] == [deviceInformation.minor intValue])
                {
                    _currentBeacon.macAddress=deviceInformation.macAddress;
                    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:TRUE],
                                                 CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
                    self.connectionTimer=[NSTimer scheduledTimerWithTimeInterval: 4.0 target:self selector:@selector(connectionTimeout:) userInfo:nil repeats:NO];
                        _connecting=YES;
                    peripheral.delegate=self;
                    _currentBeacon.peripheral=peripheral;
                    [_centralManager connectPeripheral: peripheral options:options];
                }
            }
        }
    }
    return;
}

-(void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict
{
    NSLog(@"");
}


- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    if (_beaconConnectCompletionBlock)
    {
        _beaconConnectCompletionBlock(nil, NO, error);
    }
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Connected to peripheral name: %@ and identifier:%@",peripheral.name, peripheral.identifier);
    _nbProfileInfoRead=13;
    [_connectionTimer invalidate];
    _connecting=NO;
    //NSArray *servicesUUIDs = [NSArray arrayWithObject: [CBUUID UUIDWithString: @"2013"]];
    [peripheral discoverServices: nil];
}



- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (peripheral.services == nil)
    {
        NSLog(@"--->Error, no services exposed");
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"Error")
                                                            message: NSLocalizedString(@"Error, no services exposed", @"Error, no services exposed")
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    NSLog(@"services: %d", [peripheral.services count]);
    for (CBService *aService in peripheral.services){
        NSLog(@"Service %@ discovered", aService.UUID);
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"2013"]] || [aService.UUID isEqual:[CBUUID UUIDWithString:@"180F"]]
            || [aService.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]
            || [aService.UUID isEqual:[CBUUID UUIDWithString:@"0xF000FFC0-0451-4000-B000-000000000000"]]) {
            [peripheral discoverCharacteristics:nil forService:aService];
        }
    }
}




- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"------------->didDiscoverCharacteristicsForService: %@ %d", service.UUID, service.characteristics.count);
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"2013"]] && service.characteristics.count < 9) {
        _beaconConnectCompletionBlock(nil, NO, [NSError errorWithDomain:@"Not a tBeacon dongle" code:1 userInfo:nil]);
        return;
    }
    
    for (CBCharacteristic *aChar in service.characteristics){
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconMinorValueCharacteristicUUID]]) {
            _minorValueChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconMajorValueCharacteristicUUID]]) {
            _majorValueChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconProximityUUIDCharacteristicUUID]]) {
            _proximityUUIDChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconTxPowerCharacteristicUUID]]) {
            NSLog(@"%@", aChar.UUID);
            _txPowerChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvTxPowerCharacteristicUUID]]) {
            NSLog(@"%@", aChar.UUID);
            _measuredPowerChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvIntervalCharacteristicUUID]]) {
            _advIntervalChar =aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvOffIntervalCharacteristicUUID]]) {
            _advOffIntervalChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvOnIntervalCharacteristicUUID]]) {
            _advOnIntervalChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvModeCharacteristicUUID]]) {
            _advModeChar=aChar;
            [aPeripheral readValueForCharacteristic: aChar];
        }
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: @"2A19"]]) {
            _battLevelChar=aChar;
            [aPeripheral readValueForCharacteristic: _battLevelChar];
        }
        
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: @"2A26"]]) {
            _firmwareRevisionChar=aChar;
            [aPeripheral readValueForCharacteristic: _firmwareRevisionChar];
        }
        
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: @"2A27"]]) {
            _hardwareRevisionChar=aChar;
            [aPeripheral readValueForCharacteristic: _hardwareRevisionChar];
        }
        
        if ([aChar.UUID isEqual:[CBUUID UUIDWithString: @"2A29"]]) {
            _manufacturerChar=aChar;
            [aPeripheral readValueForCharacteristic: _manufacturerChar];
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    NSLog(@"didUpdateValueForCharacteristic: %@", characteristic.UUID);
    if (!error) {
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: @"0xF000FFC1-0451-4000-B000-000000000000"]]) {
            [_oadProfile didUpdateValueForProfile: characteristic];
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconMinorValueCharacteristicUUID]]) {
            uint16_t minorValue = [self extractUInt16FromData:characteristic.value atOffset:0 andConvertFromNetworkOrder:YES];
             _currentBeacon.major= [NSNumber numberWithInt: minorValue];
            NSLog(@"MinorValue: %d", minorValue);
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconMajorValueCharacteristicUUID]]) {
            uint16_t majorValue = [self extractUInt16FromData:characteristic.value atOffset:0 andConvertFromNetworkOrder:YES];
            _currentBeacon.major= [NSNumber numberWithInt: majorValue];
            NSLog(@"MajorValue: %d", majorValue);
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconProximityUUIDCharacteristicUUID]]) {
            NSData *data = characteristic.value;
            NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDBytes: data.bytes];
            NSLog(@"%@", [proximityUUID UUIDString]);
            _currentBeacon.proximityUUID= proximityUUID;
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvTxPowerCharacteristicUUID]]) {
            char advTxPower;
            [characteristic.value getBytes:&advTxPower length:1];
            _currentBeacon.measuredPower= [NSNumber numberWithInt: advTxPower];
            NSLog(@"AdvTxPower: %d", advTxPower);
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconTxPowerCharacteristicUUID]]) {
            char txPower;
            [characteristic.value getBytes:&txPower length:1];
            _currentBeacon.txPower=txPower;
            NSLog(@"TxPower: %d", txPower);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvIntervalCharacteristicUUID]]) {
            char advInterval;
            [characteristic.value getBytes:&advInterval length:1];
            _currentBeacon.advInterval= advInterval;
            NSLog(@"advInterval: %d", advInterval);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvOffIntervalCharacteristicUUID]]) {
            char advOffInterval;
            [characteristic.value getBytes:&advOffInterval length:1];
            _currentBeacon.advOffInterval=advOffInterval;
            NSLog(@"advOffInterval: %d", advOffInterval);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: kBeaconAdvOnIntervalCharacteristicUUID]]) {
            char advOnInterval;
            [characteristic.value getBytes:&advOnInterval length:1];
            _currentBeacon.advOnInterval=advOnInterval;
            NSLog(@"advOnInterval: %d", advOnInterval);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: @"2A19"]]) {
            char batteryLevel;
            [characteristic.value getBytes:&batteryLevel length:1];
            _currentBeacon.batteryLevel=[NSNumber numberWithInt: batteryLevel];
            NSLog(@"BatteryLevel: %d", batteryLevel);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: @"2A26"]]) {
            NSData *data = characteristic.value;
            _currentBeacon.firmwareVersion=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"FirmwareRevision: %@", _currentBeacon.firmwareVersion);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: @"2A27"]]) {
            NSData *data = characteristic.value;
            _currentBeacon.hardwareVersion = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"HardwareRevision: %@", _currentBeacon.hardwareVersion);
        } else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString: @"2A29"]]) {
            NSData *data = characteristic.value;
            NSString *stringFromData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"Manufacturer: %@", stringFromData);
        }


        
        _nbProfileInfoRead--;
        
        if (_nbProfileInfoRead == 0) {
            _beaconConnectCompletionBlock(_currentBeacon, YES, nil);
        }
        
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (_beaconUpdateCompletionBlock != nil)
    {
        _beaconUpdateCompletionBlock(self, error?NO:YES, error);
    }
}


-(void)disconnectBeacon:(TICBeacon*)beacon disconnectCompletion: (TICBeaconDisconnectCompletionBlock)completion
{
    
    _beaconDisconnectCompletionBlock=completion;
    [_centralManager cancelPeripheralConnection: _currentBeacon.peripheral];
}

-(void)connectBeacon: (TICBeacon*)beacon connectCompletion: (TICBeaconConnectCompletionBlock)completion
{
    [self startMonitoring];
    _beaconConnectCompletionBlock=completion;
    _currentBeacon = beacon;
}

-(void)connectionTimeout:(NSTimer *)timer {
    _connecting = NO;
    NSLog(@"Connection timeout");
}

-(void)setMinorValue: (TICBeaconMinorValue)value forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
    uint16_t val = ntohs(value);
    NSData *data=[NSData dataWithBytes: &val length:2];
    _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_minorValueChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setMajorValue: (TICBeaconMajorValue)value forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        uint16_t val = ntohs(value);
        NSData *data=[NSData dataWithBytes: &val length:2];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_majorValueChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setMeasuredPower: (int)power forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        char newValue=(char)power;
        NSData *data=[NSData dataWithBytes: &newValue length:1];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_measuredPowerChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
    
}

-(void)setProximityUUID: (NSUUID *)proximityUUID forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        uuid_t uuid;
        [proximityUUID getUUIDBytes:uuid];
        NSData *data = [NSData dataWithBytes:uuid length:16];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_proximityUUIDChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setTxPower: (int)power forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        char newValue=(char)power;
        NSData *data=[NSData dataWithBytes: &newValue length:1];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_txPowerChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setAdvInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion:(TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        char newValue=(char)interval;
        NSData *data=[NSData dataWithBytes: &newValue length:1];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_advIntervalChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setAdvOffInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion:(TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        char newValue=(char)interval;
        NSData *data=[NSData dataWithBytes: &newValue length:1];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_advOffIntervalChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}

-(void)setAdvOnInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion:(TICBeaconManagerCompletionBlock)completion
{
    if (beacon.peripheral != nil && beacon.peripheral.state == CBPeripheralStateConnected)
    {
        char newValue=(char)interval;
        NSData *data=[NSData dataWithBytes: &newValue length:1];
        _beaconUpdateCompletionBlock = completion;
        [_currentBeacon.peripheral writeValue: data forCharacteristic:_advOnIntervalChar type: CBCharacteristicWriteWithResponse];
    } else
    {
        completion(self, NO, [NSError errorWithDomain:@"tibeacon" code:0 userInfo:nil]);
    }
}



-(void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error
{
    if (_isCalibrating)
    {
        [_calibrationRSSIs addObject: peripheral.RSSI];
        
        NSDate *now = [NSDate date];
        NSTimeInterval interval = [now timeIntervalSinceDate: _startCalibrationDate];
        
        double progress= interval / (NSTimeInterval)CALIBRATION_DURATION;
        _beaconCalibrationProgressBlock(progress);
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [peripheral readRSSI];
        });
        
    }
}




- (UInt16)extractUInt16FromData:(NSData *)data atOffset:(unsigned int)offset andConvertFromNetworkOrder:(BOOL)flag
{
	// 16 bits = 2 bytes
	
	if([data length] < offset + 2) return 0;
	
	UInt16 *pResult = (UInt16 *)([data bytes] + offset);
	UInt16 result = *pResult;
	
	if(flag)
		return ntohs(result);
	else
		return result;
}

#pragma mark -
#pragma mark UpdateFirmware
-(void)updateFirmwareForBeacon: (TICBeacon*)beacon withProgressHandler:(TICUpdateFirmwareProgressHandler)progressHandler completionHandler:(TICUpdateFirmwareCompletionBlock)completion
{
    self.beaconUpdateFirmwareProgressBlock=progressHandler;
    self.beaconUpdateFirmwareCompletionBlock=completion;
    
    BLEDevice *dev = [[BLEDevice alloc]init];
    dev.p = _currentBeacon.peripheral;
    dev.manager = _centralManager;
    self.oadProfile = [[BLETIOADProfile alloc]initWithDevice:dev];
    [self.oadProfile makeConfigurationForProfile];
    [self.oadProfile configureProfile];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"FIRMARE_UPDATED" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [[NSUserDefaults standardUserDefaults] setBool: NO forKey: @"newFirmware"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
        _beaconUpdateFirmwareCompletionBlock(nil);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"FIRMWARE_PROGRESS" object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSNumber *progress = [note.userInfo objectForKey:@"PROGRESS"];
        NSTimeInterval secondsLeft = [[note.userInfo objectForKey:@"SECONDS_LEFT"] integerValue];
        _beaconUpdateFirmwareProgressBlock([progress floatValue], secondsLeft);
    }];
}

-(void)checkFirmwareUpdateForBeacon: (TICBeacon*)beacon checkUpdateCompletion: (TICCheckFirmwareUpdateCompletionBlock)completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *firmwareVersion = beacon.firmwareVersion;
    NSString *url = [NSString stringWithFormat:@"http://ticatag.com:8280/api/firmwares/%@", firmwareVersion];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"fred" password:@"fred"];
    NSString* escapedUrlString =
    [url stringByAddingPercentEscapesUsingEncoding:
     NSUTF8StringEncoding];
    
    [manager GET: escapedUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        NSDictionary *res = (NSDictionary*)responseObject;
        NSString *version = [res objectForKey:@"version"];
        if (version != nil)
        {
            NSLog(@"New version available: %@", version);
            completion(YES, nil);
        } else{
            completion(NO, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        completion(NO, error);
    }];
}



#pragma mark -
#pragma mark Beacon Calibration
-(void)calibrateBeacon: (TICBeacon*)beacon withProgressHandler:(TICCalibrationProgressHandler)progressHandler completionHandler:(TICCalibrationCompletionBlock)completion
{
    if (_isCalibrating)
     {
     NSLog(@"Calibrating in progress");
     return;
     }
     _isCalibrating=YES;
     _beaconCalibrationProgressBlock = [progressHandler copy];
     _beaconCalibrationCompletionBlock = completion;
    _startCalibrationDate = [NSDate date];
     if (_calibrationRSSIs == nil)
     {
     _calibrationRSSIs = [NSMutableArray array];
     }
     else{
     [_calibrationRSSIs removeAllObjects];
     }

     _calibrationTimer = [[NSTimer alloc] initWithFireDate:[NSDate dateWithTimeIntervalSinceNow:CALIBRATION_DURATION] interval:0 target:self selector:@selector(calibrationTimerElapsed:) userInfo:nil repeats:NO];
     [[NSRunLoop currentRunLoop] addTimer:_calibrationTimer  forMode:NSDefaultRunLoopMode];
     
    if (_centralManager.state != CBCentralManagerStatePoweredOn) {
        return;
    }
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool: YES],
                             CBCentralManagerScanOptionAllowDuplicatesKey, nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_centralManager scanForPeripheralsWithServices: nil options:options];
    });

}

- (void)cancelCalibration
{
    @synchronized(self)
     {
     if(_isCalibrating)
     {
     _isCalibrating = NO;
     [_calibrationTimer fire];
     }
    }
}

-(void)calibrationTimerRSSIElapsed: (NSTimer*)timer
{
    @synchronized(self)
     {
     if(_isCalibrating)
     {
     
     } else{
     [timer invalidate];
     }
     }
}

-(void)calibrationTimerElapsed: (NSTimer*)timer
{
    NSLog(@"Calibration completed");
     
     NSUInteger outlierPadding = _calibrationRSSIs.count * 0.1f;
     [_calibrationRSSIs sortUsingSelector:@selector(compare:)];
     
     NSArray *sample = [_calibrationRSSIs subarrayWithRange:NSMakeRange(outlierPadding, _calibrationRSSIs.count - (outlierPadding * 2))];
     int measuredPower = [[sample valueForKeyPath:@"@avg.intValue"] integerValue];
    [_centralManager stopScan];
    _isCalibrating=NO;
    if (_beaconCalibrationCompletionBlock)
    {
        _beaconCalibrationCompletionBlock(measuredPower, nil);
    }
    
    [_calibrationRSSIs removeAllObjects];
}

@end
