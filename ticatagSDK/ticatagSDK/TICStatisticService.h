//
//  TTStatisticService.h
//  ticatagapi
//
//  Created by Fred Visticot on 7/31/13.
//  Copyright (c) 2013 ticatag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class TICBeaconHistoryRecord;
@class TICBeaconRegion;
@interface TICStatisticService : NSObject<CLLocationManagerDelegate>


@property(nonatomic) NSInteger flushModulo;

+(TICStatisticService*)shared;
-(void)start;
-(void)stop;
-(void)flush;
-(TICBeaconHistoryRecord*)createHistoricalRecord: (TICBeaconRegion*)region;
@end
