//
//  TICBeaconRegion.h
//  tibeacon
//
//  Created by Fred Visticot on 16/02/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class CLBeaconRegion;
@interface TICBeaconRegion : CLBeaconRegion

@end
