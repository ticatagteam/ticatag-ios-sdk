//
//  TICBeaconManager.h
//  tibeacon
//
//  Created by Fred Visticot on 16/02/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@class TICBeacon;
@class TICBeaconRegion;
@class TICBeaconManager;


typedef uint16_t TICBeaconMinorValue;
typedef uint16_t TICBeaconMajorValue;

typedef NS_ENUM(NSInteger, TICBeaconAdvMode) {
    TICBeaconAdvModeTBe,
    TICBeaconAdvModeIBeacon
};

typedef void(^TICBeaconManagerCompletionBlock)(TICBeaconManager *manager, BOOL success, NSError *error);
typedef void(^TICBeaconConnectCompletionBlock)(TICBeacon *beacon, BOOL success, NSError *error);
typedef void(^TICBeaconDisconnectCompletionBlock)(BOOL success, NSError *error);
typedef void (^TICCalibrationProgressHandler)(float percentComplete);
typedef void (^TICCalibrationCompletionBlock)(NSInteger measuredPower, NSError *error);
typedef void (^TICCheckFirmwareUpdateCompletionBlock)(BOOL newFirmwareAvailable, NSError *error);


typedef void (^TICUpdateFirmwareProgressHandler)(float percentComplete, NSTimeInterval remainingTime);
typedef void (^TICUpdateFirmwareCompletionBlock)(NSError *error);

@protocol TICBeaconManagerDelegate <NSObject>

- (void)beaconManager:(TICBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(TICBeaconRegion *)region;

- (void)beaconManager:(TICBeaconManager *)manager didEnterRegion:(TICBeaconRegion *)region;

- (void)beaconManager:(TICBeaconManager *)manager didExitRegion:(TICBeaconRegion *)region;

- (void)beaconManager:(TICBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(TICBeaconRegion *)region;

- (void)beaconManager:(TICBeaconManager *)manager monitoringDidFailForRegion:(TICBeaconRegion *)region withError:(NSError *)error;

- (void)beaconManager:(TICBeaconManager *)manager rangingBeaconsDidFailForRegion:(TICBeaconRegion *)region withError:(NSError *)error;

@end



@interface TICBeaconManager : NSObject<CLLocationManagerDelegate>

@property (nonatomic, weak) id <TICBeaconManagerDelegate> delegate;

+ (TICBeaconManager *)shared;

- (id) initWithCompletion: (TICBeaconManagerCompletionBlock)completion andDelegate: (id<TICBeaconManagerDelegate>)delegate;

- (void)requestStateForRegion:(TICBeaconRegion *)region;

- (void)startMonitoringForRegion:(TICBeaconRegion *)region;
- (void)stopMonitoringForRegion:(TICBeaconRegion *)region;
- (void)startRangingBeaconsInRegion:(TICBeaconRegion *)region;
- (void)stopRangingBeaconsInRegion:(TICBeaconRegion *)region;


-(void)disconnectBeacon:(TICBeacon*)beacon disconnectCompletion: (TICBeaconDisconnectCompletionBlock)completion;
-(void)connectBeacon: (TICBeacon*)beacon connectCompletion: (TICBeaconConnectCompletionBlock)block;
-(void)setMinorValue: (TICBeaconMinorValue)value forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setMajorValue: (TICBeaconMajorValue)value forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setProximityUUID: (NSUUID *)proximityUUID forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setMeasuredPower: (int)power forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setTxPower: (int)power forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setAdvInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setAdvOnInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)setAdvOffInterval: (int)interval forBeacon: (TICBeacon*)beacon withCompletion: (TICBeaconManagerCompletionBlock)completion;
-(void)calibrateBeacon: (TICBeacon*)beacon withProgressHandler:(TICCalibrationProgressHandler)progressHandler completionHandler:(TICCalibrationCompletionBlock)completion;


-(void)updateFirmwareForBeacon: (TICBeacon*)beacon withProgressHandler:(TICUpdateFirmwareProgressHandler)progressHandler completionHandler:(TICUpdateFirmwareCompletionBlock)completionHandler;
-(void)checkFirmwareUpdateForBeacon: (TICBeacon*)beacon checkUpdateCompletion: (TICCheckFirmwareUpdateCompletionBlock)completion;

@end
