//
//  TICBeacon.h
//  tibeacon
//
//  Created by Fred Visticot on 16/02/14.
//  Copyright (c) 2014 ticatag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>



typedef NS_ENUM(NSInteger, TICBeaconTxPower) {
    TICBeaconTxPowerVeryLow,
    TICBeaconTxPowerLow,
    TICBeaconTxPowerNormal,
    TICBeaconTxPowerHigh,
    TICBeaconTxPowerExperimentalLow
};

typedef NS_ENUM(NSInteger, TICBeaconAdvInterval) {
    TICBeaconAdvInterval32,
    TICBeaconAdvInterval64,
    TICBeaconAdvInterval128,
    TICBeaconAdvInterval160,
    TICBeaconAdvInterval256,
    TICBeaconAdvInterval512,
    TICBeaconAdvInterval768,
    TICBeaconAdvInterval1024,
    TICBeaconAdvInterval5120
    
};

typedef NS_ENUM(NSInteger, TICBeaconAdvOffInterval) {
    TICBeaconAdvOffInterval1000,
    TICBeaconAdvOffInterval2000,
    TICBeaconAdvOffInterval3000,
    TICBeaconAdvOffInterval4000,
    TICBeaconAdvOffInterval5000,
    TICBeaconAdvOffInterval10000
};

typedef NS_ENUM(NSInteger, TICBeaconAdvOnInterval) {
    TICBeaconAdvOnInterval1000,
    TICBeaconAdvOnInterval2000,
    TICBeaconAdvOnInterval3000,
    TICBeaconAdvOnInterval4000,
    TICBeaconAdvOnInterval5000,
    TICBeaconAdvOnInterval10000,
    TICBeaconAdvOnIntervalAlways
};

@class CBPeripheral;
@interface TICBeacon : NSObject

@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) NSNumber *major;
@property (nonatomic, strong) NSNumber *measuredPower;
@property (nonatomic, strong) NSNumber *minor;
@property (nonatomic, strong) NSNumber *power;
@property (nonatomic) CLProximity proximity;
@property (nonatomic, strong) NSUUID *proximityUUID;
@property (nonatomic) NSInteger rssi;
@property (readonly, nonatomic) CLLocationAccuracy accuracy;;
@property (nonatomic, strong) NSString *hardwareVersion;
@property (nonatomic, strong) NSString *firmwareVersion;
@property (nonatomic, strong) NSNumber *batteryLevel;
@property (nonatomic, strong) NSString *macAddress;


@property(nonatomic) TICBeaconTxPower txPower;
@property(nonatomic) TICBeaconAdvInterval advInterval;
@property(nonatomic) TICBeaconAdvOnInterval advOnInterval;
@property(nonatomic) TICBeaconAdvOffInterval advOffInterval;

-(id)initWithBeacon: (CLBeacon*)beacon;

@end
